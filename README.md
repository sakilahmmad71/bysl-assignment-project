## BYSL Assignment Project

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Features

- Upload Files (images and pdf files)
- View Files
- Delete Files
- Automatically Delete Files After 30 days if Unused

## Tech
Technologies used for this project

- JavaScript - Programming Language
- Node.js - Runtime Environment
- ExpressJS - Node.js Framework For Create API's
- JWT - Json Web Token for Public and Private Keys
- node-corn - For Delete Files After 30 Days
- Github - Source code management

## Installation

Open your terminal and simply clone this repo.

```sh
git clone https://gitlab.com/sakilahmmad71/bysl-assignment-project.git
```

Then go to the project directory.

```sh
cd bysl-assignment-project
```

requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies and devDependencies.

```sh
npm install
```

For run the application in development mode.
```sh
npm run dev
```

For run the application in production mode.
```sh
npm run server
```

Verify the server running by openning postman or insomnia and go

```sh
GET http://127.0.0.1:4000/api/v1/tests
or
GET http://localhost:4000/api/v1/tests
```
## API Usage Instructions

#### POST or Upload files (images and pdf)
Body should be 'multipart/form-data' and files should contains the key 'uploads'

```sh
POST http://localhost:4000/api/v1/files
```

#### GET Uploaded files (images and pdf)
```sh
GET http://localhost:4000/api/v1/files
```

#### GET Individual Uploaded file
```sh
GET http://localhost:4000/api/v1/files/uploads-1650213510840.jpg
```

#### DELETE or Remove Individual Uploaded file
```sh
DELETE http://localhost:4000/api/v1/files/uploads-1650196990080.jpg?privateKey=tokenshouldbehere
```