import express from 'express';
import routes from './src/routes.js';

const app = express();
const PORT = process.env.PORT || 4000;

app.use(express.static('public'));

app.use('/api', routes);

app.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Server successfully running on port ${PORT}...`);
});
