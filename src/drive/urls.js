import express from 'express';
import { MAX_NUMBERS_OF_FILES } from '../common/config/constants.js';
import isAuthenticated from '../common/middleware/isAuthenticated.js';
import multipleFiles from '../common/middleware/multipleFiles.js';
import deleteFile from './v1/deleteFile.js';
import getFile from './v1/getFile.js';
import getFiles from './v1/getFiles.js';
import postFile from './v1/postFile.js';

const files = express.Router();

files.get('/v1/files/:id', getFile);
files.get('/v1/files', getFiles);
files.post('/v1/files', multipleFiles.array('uploads', MAX_NUMBERS_OF_FILES), postFile);
files.delete('/v1/files/:id', isAuthenticated, deleteFile);

export default files;

/**
 * Following API versioning without breaking any changes
 * Can be request both v1 and v2 if implemented
 * Easily move through v1 to v2 without database migration
 * Users won't be distructed
 * Easy to maintain, manage and scale apis
 */
