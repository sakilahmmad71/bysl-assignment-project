import jwt from 'jsonwebtoken';
import { FAKE_USER, SECRET_KEY } from '../../common/config/constants.js';

const postFile = (request, response) => {
  const token = jwt.sign(FAKE_USER, SECRET_KEY);

  const files = request.files.map((file) => ({
    access: `http://localhost:4000${request.originalUrl}/${file.filename}`,
    delete: `http://localhost:4000${request.originalUrl}/${file.filename}?privateKey=${token}`,
  }));

  response.status(200).json({
    message: 'Your files successfully uploaded.',
    files,
  });
};

export default postFile;
