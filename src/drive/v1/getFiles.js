import getFileNames from '../../common/utils/getFileNames.js';
import isEmpty from '../../common/utils/isEmpty.js';

const getFiles = async (request, response) => {
  const fileDirectory = '../../../public/uploads';
  const fileNames = await getFileNames(fileDirectory);

  if (isEmpty(fileNames)) {
    return response.status(200).json({ message: 'No files found' });
  }

  return response.status(200).json({
    message: 'Files successfully found',
    files: fileNames.map((file) => `http://localhost:4000/uploads/${file}`),
  });
};

export default getFiles;
