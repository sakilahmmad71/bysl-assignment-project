import { promises as fs } from 'fs';
import path from 'path';
import __dirname from '../../common/utils/dirname.js';
import getFileNames from '../../common/utils/getFileNames.js';

// eslint-disable-next-line consistent-return
const deleteFile = async (request, response) => {
  const { id } = request.params;

  const fileDirectory = '../../../public/uploads';
  const fileNames = await getFileNames(fileDirectory);

  if (!JSON.stringify(fileNames).includes(id)) {
    return response.status(404).json({ message: `${id} - File not found` });
  }

  if (!request?.user?.id) {
    return response.status(403).json({ message: 'Permission Denied' });
  }

  try {
    await fs.unlink(path.resolve(__dirname, `../../../public/uploads/${id}`));
  } catch (error) {
    throw new Error('Cannot read directory');
  }

  response.json({ message: 'File deleted successfully' });
};

export default deleteFile;
