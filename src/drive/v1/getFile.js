import getFileNames from '../../common/utils/getFileNames.js';

const getFile = async (request, response) => {
  const { id } = request.params;

  const fileDirectory = '../../../public/uploads';
  const fileNames = await getFileNames(fileDirectory);

  if (!JSON.stringify(fileNames).includes(id)) {
    return response.status(404).json({ message: 'File Not Found' });
  }

  return response.status(200).json({ message: 'File successfully found', url: `http://localhost:4000/uploads/${id}` });
};

export default getFile;
