import multer from 'multer';

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'public/uploads');
  },
  filename(req, file, cb) {
    const filename = `${file.fieldname}-${Date.now()}${file.originalname.match(/\..*$/)[0]}`;
    cb(null, filename);
  },
});

const multipleFiles = multer({
  storage,
  limits: { fieldSize: 1024 * 1024 * 5 },
  // eslint-disable-next-line consistent-return
  fileFilter: (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg' || file.mimetype === 'application/pdf') {
      cb(null, true);
    } else {
      cb(null, false);
      const err = new Error('Only .png, .jpg .jpeg and .pdf format allowed!');
      err.name = 'ExtensionError';
      return cb(err);
    }
  },
});

export default multipleFiles;
