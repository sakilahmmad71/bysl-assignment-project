import jwt from 'jsonwebtoken';
import { SECRET_KEY } from '../config/constants.js';

// eslint-disable-next-line consistent-return
const isAuthenticated = (request, response, next) => {
  const { privateKey } = request.query;

  if (!privateKey) return response.status(401).json({ message: 'Provide Private Key' });

  // eslint-disable-next-line consistent-return
  jwt.verify(privateKey, SECRET_KEY, (err, user) => {
    if (err) return response.status(403).json({ message: 'Permission Denied' });
    request.user = user;
    next();
  });

  next();
};

export default isAuthenticated;
