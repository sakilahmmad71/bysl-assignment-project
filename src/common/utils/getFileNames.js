import { promises as fs } from 'fs';
import path from 'path';
import __dirname from './dirname.js';

const getFileNames = async (directory) => {
  let fileNames;

  try {
    fileNames = await fs.readdir(path.resolve(__dirname, directory));
  } catch (error) {
    throw new Error('Cannot read directory');
  }

  return fileNames;
};

export default getFileNames;
