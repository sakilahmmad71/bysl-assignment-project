import express from 'express';
import files from './drive/urls.js';
import tests from './test/urls.js';

const routes = express.Router();

routes.use(tests);
routes.use(files);

export default routes;
