import express from 'express';
import getTest from './v1/getTest.js';

const tests = express.Router();

tests.get('/v1/tests', getTest);

export default tests;
